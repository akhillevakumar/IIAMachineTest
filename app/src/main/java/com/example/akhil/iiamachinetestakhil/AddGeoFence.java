package com.example.akhil.iiamachinetestakhil;

import android.Manifest;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import com.google.android.gms.location.LocationListener;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AddGeoFence extends AppCompatActivity implements LocationListener {
    PendingIntent mGeofencePendingIntent;
    private List<Geofence> mGeofenceList;
    private GoogleApiClient mGoogleApiClient;
    public static final String TAG = "Activity";
    LocationRequest mLocationRequest;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    //from sp//
    double fenceLatitude , fenceLongitude ;
    TextView latText;
    TextView logText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_geo_fence);
        sharedPreferences=getSharedPreferences("Location",MODE_PRIVATE);
        String lat= sharedPreferences.getString("lat","0.0");
        String log =sharedPreferences.getString("log","0.0");
        initView();
        latText.setText(lat);
        logText.setText(log);

        fenceLatitude=Double.parseDouble(lat);
        fenceLongitude=Double.parseDouble(log);
       // Toast.makeText(this, ""+lat+log, Toast.LENGTH_SHORT).show();
        if (savedInstanceState == null) {

            mGeofenceList = new ArrayList<Geofence>();

            int resp = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            if (resp == ConnectionResult.SUCCESS) {



                //

            } else {
                Log.e(TAG, "Your Device doesn't support Google Play Services.");
            }
findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        String Rad;
        EditText editText =(EditText)findViewById(R.id.radius);
                Rad=editText.getText().toString().trim();


        if (Rad.isEmpty()){
            Toast.makeText(AddGeoFence.this, "Enter a value", Toast.LENGTH_SHORT).show();
        }
        else {
            editor=sharedPreferences.edit();
            editor.putString("radius",Rad);
            int rad=Integer.parseInt(Rad);
            if (rad >= 100 && rad <= 1000) {
                initGoogleAPIClient();
                createGeofences(fenceLatitude, fenceLongitude, rad);
                /*Toast.makeText(AddGeoFence.this, " GeoFence added you will be notified " +
                        "" +
                        "when this device crosses the fence", Toast.LENGTH_SHORT).show();*/
                AlertDialog.Builder builder=new AlertDialog.Builder(AddGeoFence.this);
                builder.setTitle("GeoFence added");
                builder.setMessage("GeoFence added you will be notified when this device crosses the fence");
                builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(AddGeoFence.this,MapsActivity.class));
                        finish();

                    }

                });builder.show();

            } else {
                Toast.makeText(AddGeoFence.this, "100-1000", Toast.LENGTH_SHORT).show();
            }
        }
    }
});

            mLocationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(1 * 1000)        // 10 seconds, in milliseconds
                    .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        }

    }

    public void initView() {
       latText =(TextView)findViewById(R.id.lat);

       logText =(TextView)findViewById(R.id.log);
    }

    public void initGoogleAPIClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(connectionAddListener)
                .addOnConnectionFailedListener(connectionFailedListener)
                .build();
        mGoogleApiClient.connect();
    }

    private GoogleApiClient.ConnectionCallbacks connectionAddListener =
            new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(Bundle bundle) {
                    Log.i(TAG, "onConnected");

                    if (ActivityCompat.checkSelfPermission(AddGeoFence.this, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(AddGeoFence.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                    if (location == null) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,AddGeoFence.this);

                    } else {

                        fenceLatitude  = location.getLatitude();
                        fenceLongitude = location.getLongitude();

                        //Log.i(TAG, fenceLatitude + "  " + fenceLongitude);

                    }

                    try{
                        LocationServices.GeofencingApi.addGeofences(
                                mGoogleApiClient,
                                getGeofencingRequest(),
                                getGeofencePendingIntent()
                        ).setResultCallback(new ResultCallback<Status>() {

                            @Override
                            public void onResult(Status status) {
                                if (status.isSuccess()) {
                                    Log.i(TAG, "Saving Geofence");

                                } else {
                                    Log.e(TAG, "Registering geofence failed: " + status.getStatusMessage() +
                                            " : " + status.getStatusCode());
                                }
                            }
                        });

                    } catch (SecurityException securityException) {

                        Log.e(TAG, "Error");
                    }
                }

                @Override
                public void onConnectionSuspended(int i) {

                    Log.e(TAG, "onConnectionSuspended");

                }
            };

    private GoogleApiClient.OnConnectionFailedListener connectionFailedListener =
            new GoogleApiClient.OnConnectionFailedListener() {
                @Override
                public void onConnectionFailed(ConnectionResult connectionResult) {
                    Log.e(TAG, "onConnectionFailed");
                }
            };

    /**
     * Create a Geofence list
     */
    public void createGeofences(double latitude, double longitude,int radius) {
        String id = UUID.randomUUID().toString();
        Geofence fence = new Geofence.Builder()
                .setRequestId(id)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .setCircularRegion(latitude, longitude, radius)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .build();
        mGeofenceList.add(fence);
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, FenceIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        return PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onLocationChanged(Location location) {
       fenceLatitude = location.getLatitude();
        fenceLongitude = location.getLongitude();
        Log.i(TAG, "onLocationChanged");
    }



}
